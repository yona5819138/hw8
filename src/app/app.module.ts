import { AppComponent } from './app.component';
import { NavComponent } from './nav/nav.component';
import { SingupComponent } from './singup/singup.component';
import { WelcomeComponent } from './welcome/welcome.component';
import { HomeComponent } from './home/home.component';

import { AngularFireModule } from '@angular/fire';
import { AngularFireDatabaseModule } from '@angular/fire/database';
import { AngularFireAuthModule } from '@angular/fire/auth';
import {environment} from '../environments/environment';



import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { Routes, RouterModule } from '@angular/router';

import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {FormsModule} from '@angular/forms';
import {MatButtonModule,MatCheckboxModule,MatToolbarModule,MatInputModule,MatProgressSpinnerModule,MatCardModule,MatMenuModule, MatIconModule} from '@angular/material';
import {MatTableModule} from '@angular/material/table';
import {MatSelectModule } from '@angular/material/select';
import {ReactiveFormsModule} from '@angular/forms';
import { LoginComponent } from './login/login.component';
import { TodoComponent } from './todo/todo.component';

@NgModule({
  declarations: [
    AppComponent,
    NavComponent,
    SingupComponent,
    WelcomeComponent,
    HomeComponent,
    LoginComponent,
    TodoComponent,
    
  ],
  imports: [
    BrowserModule,
    MatButtonModule,
    BrowserAnimationsModule,
    MatInputModule,
    MatToolbarModule,
    MatCardModule,
    FormsModule,
    MatIconModule, 
    AngularFireModule.initializeApp(environment.firebase),    
    AngularFireDatabaseModule,
    AngularFireAuthModule,
    ReactiveFormsModule,
    MatCheckboxModule,
    MatSelectModule,

 
    
    RouterModule.forRoot([
      {path:'',component:SingupComponent},
      {path:'welcome',component:WelcomeComponent},
      {path:'home',component:HomeComponent},
      {path:'Singup',component:SingupComponent},
      {path:'login',component:LoginComponent},
      {path:'**',component:SingupComponent}

    ])
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }