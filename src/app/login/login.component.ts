import { Component, OnInit } from '@angular/core';
import{AuthService} from '../auth.service';
import { Router } from '@angular/router';

@Component({
  selector: 'login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  email: string;
  password: string;
 
  code= '';
  message= '';

  required ='';
  


  login(){

      if (this.password == null || this.email == null) 
    {
      this.required = "this input is required";
    }



    this.authService.login(this.email, this.password)
        .then(user => {
              this.router.navigate(['/welcome']);
             }).catch(err => {
                this.code = err.code;
                this.message = err.message;
                console.log(err);
              })
              
  }
  constructor(public authService:AuthService,private router:Router) { }

  ngOnInit() {
  }

 

}
