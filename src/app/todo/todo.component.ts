import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { TodosService } from '../todos.service';

@Component({
  selector: 'todo',
  templateUrl: './todo.component.html',
  styleUrls: ['./todo.component.css']
})

export class TodoComponent implements OnInit {
  @Input() data:any;
  @Output() myButtonClicked= new EventEmitter<any>();
  text;
  tempText;
  key;
  checkboxFlag: boolean;

showTheButton = false;
showEditField = false;

send(){
  console.log('event caught');
  this.myButtonClicked.emit(this.text);
}

showButton(){
this.showTheButton = true; 
}
hideButton(){
  this.showTheButton = false;

}
deleteTodo(){
  this.todosService.deleteTodo(this.key);
}

showEdit(){
  this.showEditField = true;
  this.tempText = this.text;
  this.showTheButton =true;
}

save(){
this.todosService.update(this.key,this.text,this.checkboxFlag);
this.showEditField = false;
}

cancel(){
  this.showEditField=false;
  this.text= this.tempText;
}
checkChange()
{
  this.todosService.updateDone(this.key,this.text,this.checkboxFlag);
}

  constructor(public todosService: TodosService) { }

  ngOnInit() {
    this.text = this.data.text;
    this.key = this.data.$key;
    this.checkboxFlag = this.data.done;

  }
  

}
