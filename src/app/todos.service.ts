import { Injectable } from '@angular/core';
import { AuthService } from './auth.service';
import { AngularFireDatabase } from '@angular/fire/database';

@Injectable({
  providedIn: 'root'
})
export class TodosService {

  constructor(private authService: AuthService, private db: AngularFireDatabase) { }

  addTodo(text:string, done:boolean)
  {
    this.authService.user.subscribe(user => {
      this.db.list('/users/'+user.uid+'/todos').push({'text':text, 'done':false});//מוסיף טודו לכל יוזר מסוים
    })
  }

  // updateDone(key:string, text:string, done:boolean)
  // {
  //   this.authService.user.subscribe(user =>{
  //     this.db.list('/users/'+user.uid+'/todos').update(key,{'text':text, 'done':done});
  //   })
    
  // }

  deleteTodo(key:string)
  {
    this.authService.user.subscribe(user => {
      this.db.list('/users/'+user.uid+'/todos').remove(key);
    })
  }

 
  update(key:string, text:string, done:boolean){
    this.authService.user.subscribe(user =>{
      this.db.list('/users/'+user.uid+'/todos').update(key,{'text':text, 'done':done});
    })
    
  }
  updateDone(key:string, text:string, done:boolean)
  {
    this.authService.user.subscribe(user =>{
      this.db.list('/users/'+user.uid+'/todos').update(key,{'text':text, 'done':done});
    })
    
  }
}