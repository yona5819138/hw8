import { Component, OnInit } from '@angular/core';
import { AuthService } from '../auth.service';
import { AngularFireDatabase } from '@angular/fire/database';
import { TodosService } from '../todos.service';

@Component({
  selector: 'welcome',
  templateUrl: './welcome.component.html',
  styleUrls: ['./welcome.component.css']
})
export class WelcomeComponent implements OnInit {
  todoTextFromTodo='NO TEXT TO SHOW!';

  todos=[];
  taskStatus = [];


  todo = '';
  key = this.todos['$key'];
  checkboxFlag: boolean;
  done: string;

  addTodo(){
    this.todosService.addTodo(this.todo,this.checkboxFlag);//שולח לאוט לפונקצייה שתוסיף טודו
    this.todo = '';//איפוס הטודו
    this.checkboxFlag=false;//לא בוצע עדיין
  }
  toFilter()
  {
    this.authService.user.subscribe(user => {
    this.db.list('/users/'+user.uid+'/todos').snapshotChanges().subscribe(
      todos =>{
        this.todos = [];
        todos.forEach(
          todo => {
            let y = todo.payload.toJSON();
            y["$key"] = todo.key; 
            console.log('aa'+y["$key"]);         
              if (y['done'] == this.checkboxFlag) {
                this.todos.push(y);
              }
              else if (this.checkboxFlag != true && this.checkboxFlag != false)
              {
                this.todos.push(y);
                console.log("todos"+this.todos);
              }
              
          }
        )
      }
    )
    })
  }

  // checkChange()
  // {
  //   this.todosService.updateDone(this.key, this.todo, this.checkboxFlag);
  // }

  constructor(public authService:AuthService, 
              private db: AngularFireDatabase, 
              public todosService: TodosService) { }

  
              
  ngOnInit() {
    this.authService.user.subscribe(user => {
      this.db.list('/users/'+user.uid+'/todos').snapshotChanges().subscribe(
        todos =>{
          this.todos = [];
          this.taskStatus = [];
          todos.forEach(
            todo => {
              let y =todo.payload.toJSON();
              y["$key"] = todo.key;
              this.todos.push(y);
              let sta = y['done'];
              if (this.taskStatus.indexOf(sta)== -1)
               {
               this.taskStatus.push(sta);
              }
             
            }
          )
        }
      ) 
    })
  

  }

}